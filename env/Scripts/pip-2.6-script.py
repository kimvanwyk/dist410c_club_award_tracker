#!k:\dist410c_club_award_tracker\env\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'pip==1.1','console_scripts','pip-2.6'
__requires__ = 'pip==1.1'
import sys
from pkg_resources import load_entry_point

sys.exit(
   load_entry_point('pip==1.1', 'console_scripts', 'pip-2.6')()
)
