:: Copyright (c) 2011, Kim van Wyk 
:: All rights reserved.  
:: 
:: Redistribution and use in source and binary forms, with or without
:: modification, are permitted provided that the following conditions are
:: met:
:: 
:: Redistributions of source code must retain the above copyright
:: notice, this list of conditions and the following disclaimer. 
:: Redistributions in binary form must reproduce the above copyright
:: notice, this list of conditions and the following disclaimer in the
:: documentation and/or other materials provided with the distribution.
:: 
:: THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
:: "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
:: LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
:: A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
:: HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
:: SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
:: LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
:: DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
:: THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
:: (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
:: OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

python manage.py schemamigration dist410c_club_award_tracker_app --auto
:: Check that worked
if ERRORLEVEL 1 goto fail
:: perform migration
python manage.py migrate dist410c_club_award_tracker_app
if ERRORLEVEL 1 goto fail

git add dist410c_club_award_tracker_app/migrations/*.py
if ERRORLEVEL 1 goto fail
goto pass

:fail
echo Error occured, failed output
goto eof

:pass
echo Migration success!

:eof