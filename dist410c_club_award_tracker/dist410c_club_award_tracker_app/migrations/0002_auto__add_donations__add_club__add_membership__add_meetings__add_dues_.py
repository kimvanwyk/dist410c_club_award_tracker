# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Donations'
        db.create_table('dist410c_club_award_tracker_app_donations', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('mjf', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('lcif', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('lh_corporate', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('lh_personal', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('lh_contributing_club', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Donations'])

        # Adding model 'Club'
        db.create_table('dist410c_club_award_tracker_app_club', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('pu101_in_time', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('midyear_attendance', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('midyear_membership', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('conv_attendance', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('conv_membership', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('own_website', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Club'])

        # Adding model 'Membership'
        db.create_table('dist410c_club_award_tracker_app_membership', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('new', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('reinstated', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('dropped', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('start_membership', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('end_membership', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Membership'])

        # Adding model 'Meetings'
        db.create_table('dist410c_club_award_tracker_app_meetings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('lp_attendance', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ls_attendance', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lt_attendance', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('gen_attendance', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('gen_membership', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Meetings'])

        # Adding model 'Dues'
        db.create_table('dist410c_club_award_tracker_app_dues', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('first_dist', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('first_int', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('second_dist', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('second_int', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Dues'])

        # Adding model 'Monthly'
        db.create_table('dist410c_club_award_tracker_app_monthly', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('month', self.gf('django.db.models.fields.IntegerField')()),
            ('mmr_online', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('mmr_emailed', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('minutes_emailed', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('meeting', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('activity_report_online', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('activity_report_emailed', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('newsletter_article', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('club_bulletin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('newspaper_clipping', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Monthly'])

        # Adding model 'Projects'
        db.create_table('dist410c_club_award_tracker_app_projects', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('peace_poster', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('leo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lioness', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lobs', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cakes', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('environment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sight', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hearing', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('diabetes', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ye', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('food_sec', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('youth', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['Projects'])

        # Adding model 'NewClubs'
        db.create_table('dist410c_club_award_tracker_app_newclubs', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('club_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dist410c_club_award_tracker_app.Club'])),
            ('new', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('branch', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('campus', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal('dist410c_club_award_tracker_app', ['NewClubs'])


    def backwards(self, orm):
        # Deleting model 'Donations'
        db.delete_table('dist410c_club_award_tracker_app_donations')

        # Deleting model 'Club'
        db.delete_table('dist410c_club_award_tracker_app_club')

        # Deleting model 'Membership'
        db.delete_table('dist410c_club_award_tracker_app_membership')

        # Deleting model 'Meetings'
        db.delete_table('dist410c_club_award_tracker_app_meetings')

        # Deleting model 'Dues'
        db.delete_table('dist410c_club_award_tracker_app_dues')

        # Deleting model 'Monthly'
        db.delete_table('dist410c_club_award_tracker_app_monthly')

        # Deleting model 'Projects'
        db.delete_table('dist410c_club_award_tracker_app_projects')

        # Deleting model 'NewClubs'
        db.delete_table('dist410c_club_award_tracker_app_newclubs')


    models = {
        'dist410c_club_award_tracker_app.club': {
            'Meta': {'object_name': 'Club'},
            'conv_attendance': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'conv_membership': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'midyear_attendance': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'midyear_membership': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'own_website': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pu101_in_time': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'dist410c_club_award_tracker_app.donations': {
            'Meta': {'object_name': 'Donations'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lcif': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'lh_contributing_club': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lh_corporate': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'lh_personal': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'mjf': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.dues': {
            'Meta': {'object_name': 'Dues'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'first_dist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_int': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'second_dist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'second_int': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'dist410c_club_award_tracker_app.meetings': {
            'Meta': {'object_name': 'Meetings'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'gen_attendance': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'gen_membership': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lp_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ls_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lt_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'dist410c_club_award_tracker_app.membership': {
            'Meta': {'object_name': 'Membership'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'dropped': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'end_membership': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'reinstated': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'start_membership': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.monthly': {
            'Meta': {'object_name': 'Monthly'},
            'activity_report_emailed': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'activity_report_online': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_bulletin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'minutes_emailed': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'mmr_emailed': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'mmr_online': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'month': ('django.db.models.fields.IntegerField', [], {}),
            'newsletter_article': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'newspaper_clipping': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'dist410c_club_award_tracker_app.newclubs': {
            'Meta': {'object_name': 'NewClubs'},
            'branch': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'campus': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.projects': {
            'Meta': {'object_name': 'Projects'},
            'cakes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'diabetes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'environment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'food_sec': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hearing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lioness': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lobs': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'peace_poster': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sight': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ye': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'youth': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['dist410c_club_award_tracker_app']