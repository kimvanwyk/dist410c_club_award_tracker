# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Meetings.meeting'
        db.add_column('dist410c_club_award_tracker_app_meetings', 'meeting',
                      self.gf('django.db.models.fields.CharField')(default='ZONE1', max_length=20),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Meetings.meeting'
        db.delete_column('dist410c_club_award_tracker_app_meetings', 'meeting')


    models = {
        'dist410c_club_award_tracker_app.club': {
            'Meta': {'object_name': 'Club'},
            'conv_attendance': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'conv_membership': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'midyear_attendance': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'midyear_membership': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'own_website': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pu101_in_time': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'dist410c_club_award_tracker_app.donations': {
            'Meta': {'object_name': 'Donations'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lcif': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'lh_contributing_club': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lh_corporate': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'lh_personal': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'mjf': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.dues': {
            'Meta': {'object_name': 'Dues'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'first_dist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_int': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'second_dist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'second_int': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'dist410c_club_award_tracker_app.meetings': {
            'Meta': {'object_name': 'Meetings'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'gen_attendance': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'gen_membership': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lp_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ls_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lt_attendance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meeting': ('django.db.models.fields.CharField', [], {'default': "'ZONE1'", 'max_length': '20'})
        },
        'dist410c_club_award_tracker_app.membership': {
            'Meta': {'object_name': 'Membership'},
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'dropped': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'end_membership': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'reinstated': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'start_membership': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.monthly': {
            'Meta': {'object_name': 'Monthly'},
            'activity_report_emailed': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'activity_report_online': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_bulletin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'minutes_emailed': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'mmr_emailed': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'mmr_online': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'month': ('django.db.models.fields.IntegerField', [], {}),
            'newsletter_article': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'newspaper_clipping': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'dist410c_club_award_tracker_app.newclubs': {
            'Meta': {'object_name': 'NewClubs'},
            'branch': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'campus': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'dist410c_club_award_tracker_app.projects': {
            'Meta': {'object_name': 'Projects'},
            'cakes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'club_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dist410c_club_award_tracker_app.Club']"}),
            'diabetes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'environment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'food_sec': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hearing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lioness': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lobs': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'peace_poster': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sight': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ye': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'youth': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['dist410c_club_award_tracker_app']