# Create your views here.

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import models
from django import forms
from misc_view_funcs import HorizRadioSelect, render_wrap
from django.shortcuts import redirect

def club_details(request, club_id):
    class ClubForm(forms.ModelForm):
        class Meta:
            model = models.Club
            exclude = ['name', 'year']
            field_names = model._meta.get_all_field_names()
            widgets = {}
            for w in ['midyear_attendance', 'midyear_membership', 'conv_attendance', 'conv_membership']:
                if w in field_names:
                    widgets[w] = forms.TextInput(attrs={'size':3})

    club = models.Club.objects.get(pk=club_id)
    if request.method == "POST":
        club_form = ClubForm(request.POST, instance=club)
        if form.is_valid(): # All validation rules pass
            form.save()
            return HttpResponse('Club details added')

    else:
        club_form = ClubForm(instance=club)
    
    return render_wrap('club_details.html', request, {'club_form': club_form, 'name':club.name, 'year':club.year, 'id':club_id})
