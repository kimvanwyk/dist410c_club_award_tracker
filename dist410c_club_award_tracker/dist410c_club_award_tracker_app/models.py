'''
Copyright (c) 2013, Kim van Wyk 
All rights reserved.  

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 
Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
from django.db import models
import datetime

MONTHS = ('July', 'August', 'September', 'October', 'November', 'December', 'January', 'February', 'March', 'April', 'May', 'June')

class Club(models.Model):
    name = models.CharField('Club Name', max_length=100)
    year = models.IntegerField('Lionistic year')
    pu101_in_time = models.BooleanField('PU101 submitted before 15 April')
    midyear_attendance = models.IntegerField('Number of members who attended Midyear', blank=True, default=0)
    midyear_membership = models.IntegerField('Club membership during Midyear month', blank=True, default=0)
    conv_attendance = models.IntegerField('Number of members who attended Convention', blank=True, default=0)
    conv_membership = models.IntegerField('Club membership during Convention month', blank=True, default=0)
    own_website = models.BooleanField('Club has their own and updated website', blank=True)

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return self.name

class Monthly(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")
    month = models.IntegerField('Month') # Use 1 for Jul, up to 12 for Jun
    mmr_online = models.DateField('Date MMR was submitted online', blank=True, null=True)
    mmr_emailed = models.DateField('Date MMR was emailed to Cabinet', blank=True, null=True)
    minutes_emailed = models.DateField('Date minutes were emailed to Cabinet', blank=True, null=True)
    meeting = models.DateField('Date of meeting', blank=True, null=True)
    activity_report_online = models.BooleanField('Whether Activity Report was submitted online or not')
    activity_report_emailed = models.DateField('Date Activity Report was emailed to Cabinet', blank=True, null=True)
    newsletter_article = models.BooleanField("Whether an article was submitted for the DG's newsletter")
    club_bulletin = models.BooleanField("Whether the club's bulletin was submitted to the DCS")
    newspaper_clipping = models.BooleanField("Whether a newspaper clipping was submitted to the DC Marketing")

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return '%s: %d' (self.club_id.name, MONTHS[self.month-1])

class Projects(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")    
    peace_poster = models.BooleanField("Whether the club participated in a Peace Poster project")
    leo = models.BooleanField("Whether the club participated in a Leo project")
    lioness = models.BooleanField("Whether the club participated in a Lioness project")
    lobs = models.BooleanField("Whether the club participated in a LOBS project")
    cakes = models.BooleanField("Whether the club participated in a Christmas Cakes project")
    environment = models.BooleanField("Whether the club participated in an Environment project")
    sight = models.BooleanField("Whether the club participated in a Sight project")
    hearing = models.BooleanField("Whether the club participated in a Hearing project")
    diabetes = models.BooleanField("Whether the club participated in a Diabetes project")
    ye = models.BooleanField("Whether the club participated in a YE project")
    food_sec = models.BooleanField("Whether the club participated in a Food Security project")
    youth = models.BooleanField("Whether the club participated in a Youth project")

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'Projects for %s' (self.club_id.name)

class Membership(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")    
    new = models.IntegerField("New members", blank=True, default=0)
    reinstated = models.IntegerField("Reinstated members", blank=True, default=0)
    dropped = models.IntegerField("Dropped members", blank=True, default=0)
    start_membership = models.IntegerField("Membership at the start of the year", blank=True, default=0)
    end_membership = models.IntegerField("Membership at the end of the year", blank=True, default=0)

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'Membership for %s' (self.club_id.name)

class Meetings(models.Model):
    ZONE1 = 'ZONE1'
    ZONE2 = 'ZONE2'
    ZONE3 = 'ZONE3'
    TRAINING = 'TRAINING'
    MEETING_NAMES = (
        (ZONE1, 'First Zone Meeting'),
        (ZONE2, 'Second Zone Meeting'),
        (ZONE3, 'Third Zone Meeting'),
        (TRAINING, 'Training Meeting')
        )
    club_id = models.ForeignKey(Club, verbose_name="Club")
    lp_attendance = models.BooleanField("Did LP or LPE attend?")
    ls_attendance = models.BooleanField("Did LS or LSE attend?")
    lt_attendance = models.BooleanField("Did LT or LTE attend?")
    gen_attendance = models.IntegerField('Number of non-office holding members who attended the meeting', blank=True, default=0)
    gen_membership = models.IntegerField('Club membership during meeting month', blank=True, default=0)
    meeting = models.CharField('Name of Meeting', choices=MEETING_NAMES, default=ZONE1, max_length=20)
    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'Meetings for %s' (self.club_id.name)

class NewClubs(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")
    new = models.IntegerField("Number of new clubs formed", blank=True, default=0)
    branch = models.IntegerField("Number of branch clubs formed", blank=True, default=0)
    campus = models.IntegerField("Number of campus clubs formed", blank=True, default=0)

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'New Clubs for %s' (self.club_id.name)

class Dues(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")
    first_dist = models.BooleanField("District Dues paid by 5 September")
    first_int = models.BooleanField("International Dues paid by 5 September")
    second_dist = models.BooleanField("District Dues paid by 5 March")
    second_int = models.BooleanField("International Dues paid by 5 March")

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'Dues for %s' (self.club_id.name)

class Donations(models.Model):
    club_id = models.ForeignKey(Club, verbose_name="Club")
    mjf = models.IntegerField("Number of Melvin Jones Fellowships awarded", blank=True, default=0)
    lcif = models.IntegerField("Number of LCIF contributions made", blank=True, default=0)
    lh_corporate = models.IntegerField("Number of Corporate Louis Halse Fellowships awarded", blank=True, default=0)
    lh_personal = models.IntegerField("Number of Personal Louis Halse Fellowships awarded", blank=True, default=0)
    lh_contributing_club = models.BooleanField("Is club a 100% Contributing Club to Louis Halse")

    class Meta:
        app_label = 'dist410c_club_award_tracker_app'

    def __unicode__(self):
        return 'Donations for %s' (self.club_id.name)
