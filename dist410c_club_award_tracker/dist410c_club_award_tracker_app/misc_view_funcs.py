''' Miscellaneous Django view functions
'''

from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.forms import RadioSelect
from django.utils.safestring import mark_safe

def render_wrap(template, request, args=None):
    ''' A wrapper for render_to_response which applies  csrf token automatically
    
    template and args are the arguments as given to render_to_response - a template file
    and an arguments dict
    request is the request object
    '''
    if not args:
        args = {}

    args.update(csrf(request))
    return render_to_response(template, args)

class HorizRadioRenderer(RadioSelect.renderer):
    """ this overrides a widget to put radio buttons horizontally
        instead of vertically.

        Modified from this Django Snippet: http://djangosnippets.org/snippets/2696/
    """
    def render(self):
        ''' Put radio buttons in a horizontal table, replacing '--------' with 'No answer provided'
        
        '''
        # disable swapping as the now answer option is still the checked one, regardless of order
        ws = [w for w in self]
        if 0:
            # swap the "no Answer" option from the front to the back of the list
            ws = ws[1:] + [ws[0]]
        return mark_safe(u'<table><tr>%s</tr></table>' % (''.join('<td>%s</td>' % w for w in ws)).replace('---------', 'No answer provided'))

class HorizRadioSelect(RadioSelect):
    ''' From this Django Snippet: http://djangosnippets.org/snippets/2696/'''
    renderer = HorizRadioRenderer

