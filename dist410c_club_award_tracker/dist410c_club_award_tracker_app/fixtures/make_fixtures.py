''' Create a set of fixtures to populate the database with clubs and suitable associated entries

Author: Kim van Wyk

Copyright (c) 2011, Kim van Wyk 
All rights reserved.  

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 
Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''
import json

FN = "fixtures.json"
CLUBS = ["Amanzimtoti", "Ballito", "Bloemfontein", "Cowies Hill", "Durban Central", "Durban Host", "Durban South", "East Coast", "Empangeni", "Eshowe", "Estcourt", "Ethekwini South", "Gillits-Camperdown", "Hibberdene", "Hilton", "Howick", "Kimberley", "Kingsburgh", "Kloof", "Kokstad", "Ladysmith", "North Durban", "Pietermaritzburg", "Pinetown", "Port Shepstone", "Queensburgh", "Ramsgate", "Reitz", "Scottburgh", "Shelly Beach", "Umgeni", "Virginia", "Vryheid", "Welkom", "Westville"]

# start and exclusive end range for years
YEARS = (2012, 2022)

def make_club_fixture(club, year, club_pk=[1], month_pk=[1], meeting_pk=[1]):
    ''' Return a dict of fixture details for a particular club and year
    club_, month_ and meeting_ pk are lists to allow internal modification
    '''
    
    d = []
    # Add Club details
    d.append({"model":"dist410c_club_award_tracker_app.club",
              "pk":club_pk[0],
              "fields":{"name":club,
                        "year":year}
              })
    # Add 12 months
    for m in xrange(1,13):
        d.append({"model":"dist410c_club_award_tracker_app.monthly",
                  "pk":month_pk[0],
                  "fields":{"club_id":club_pk[0],
                            "month":m}
                  })
        month_pk[0] += 1
        
    # Add projects, membership, newclubs, dues and donations, since they re all the same
    for model in ("projects", "membership", "newclubs", "dues", "donations"):
        d.append({"model":"dist410c_club_award_tracker_app.%s" % model,
                  "pk": club_pk[0],
                  "fields": {"club_id": club_pk[0]}
                  })

    # Add all 4 meetings
    for meeting in ('First Zone Meeting', 'Second Zone Meeting', 'Third Zone Meeting', 'Training Meeting'):
        d.append({"model":"dist410c_club_award_tracker_app.meetings",
                  "pk":meeting_pk[0],
                  "fields":{"club_id":club_pk[0],
                            "meeting":meeting}
                  })
        meeting_pk[0] += 1
    club_pk[0] += 1
    return d

if __name__ == "__main__":
    out = []
    with open(FN, 'w') as fh:
        for club in CLUBS:
            for year in xrange(*YEARS):
                out.extend(make_club_fixture(club, year))
        json.dump(out, fh, indent=3)
